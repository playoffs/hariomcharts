export const YEARLY_BITCOIN_VALUE = [
    { value: 10000, year: 2011 },
    { value: 15000, year: 2012 },
    { value: 20000, year: 2013 },
    { value: 25000, year: 2014 },
    { value: 30000, year: 2015 },
    { value: 25000, year: 2016 },
    { value: 15000, year: 2017 },
    { value: 35000, year: 2018 },
    { value: 10000, year: 2019 },
    { value: 24000, year: 2020 },
];
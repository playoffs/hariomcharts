import React, { Component } from "react";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    Tooltip,
    Label,
    LabelList
} from "recharts";
import './Linechart.css';

// Subscribe The Data from API's
import { YEARLY_BITCOIN_VALUE } from './bitcoinData'

class Linechart extends Component {
    render() {
        return <>
            <div>
                <h3>Yearly Bitcoin Value</h3>
                <div className="chart-container">
                    <LineChart
                        width={800}
                        height={400}
                        data={YEARLY_BITCOIN_VALUE}>
                        <Line
                            type="monotone"
                            dataKey="value"
                            stroke="#8884d8" />
                        <XAxis dataKey="year" >
                            <Label
                                value="Years"
                                offset={-2}
                                position="insideBottom" />
                        </XAxis>
                        <YAxis
                            label={{
                                value: 'Bitcoin value in US $',
                                offset: -2,
                                angle: -90,
                                position: 'left'
                            }} />
                        <Tooltip
                            formatter={(value) => new Intl.NumberFormat('en').format(value)}
                            cursor={{ stroke: 'red', strokeWidth: 2 }}
                        />
                    </LineChart>
                </div>
            </div>
        </>
    }
}

export default Linechart;

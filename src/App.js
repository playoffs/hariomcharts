import './App.css';
import Linechart from './Linechart/Linechart'
function App() {
  return (
    <div className="App">
      <Linechart />
    </div>
  );
}

export default App;
